(function() {
  'use strict';

  angular.module('app').config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('login', {
      url: '/',
      component: 'login',
    })
    .state('products', {
      name: 'products',
      url: '/products',
      templateUrl: 'app/products/products.view.html'
    })
    .state('products.list', {
      url: '/list',
      component: 'listProducts'
    })
    .state('products.create', {
      url: '/create',
      component: 'createProducts'
    });

    $urlRouterProvider.otherwise('/');
  }

})();
