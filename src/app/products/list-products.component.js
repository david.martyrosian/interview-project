(function() {
  'use strict';

  angular
    .module('app')
    .component('listProducts', {
      controller: ProductsController,
      controllerAs: 'vm',
      templateUrl: 'app/products/list-products.html',
    });

  /** @ngInject */
  ProductsController.$inject = ['dataservice'];
  function ProductsController(dataservice) {
    const vm = this;
    vm.currency = 'UAH';

    vm.loadProductsList = loadProductsList;
    vm.deleteItem = deleteItem;
    vm.setCurrency = setCurrency;

    vm.products = [];
    activate();

    function activate() {
      vm.loadProductsList();
    }

    function loadProductsList() {
      dataservice.getProducts().then(products => {
        products.forEach(pr => {
          pr.price = pr.items[0].price;
        });
       vm.products =  vm.products.concat(products);
      });
    }

    function deleteItem(product) {
      dataservice.deleteProduct(product._id).then(() => {
       vm.products.some((p, ind, arr) => {
         if (p._id === product._id) {
           arr.splice(ind, 1);
           return true;
         }
       });
      });
    }

    function setCurrency(currency) {
      vm.currency = currency;
    }
  }

})();