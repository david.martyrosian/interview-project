(function() {
  'use strict';

  angular
    .module('app')
    .directive('productItem', productItem);

  function productItem() {
    const directive = {
      require: '^listProducts',
      templateUrl: 'app/products/product.html',
      restrict: 'E',
      scope: {
        product: '<',
        currency: '<'
      },
      link
    };
    return directive;
  }

  function link(scope, element, attr, ProductsCtrl) {
    element.css({
     'background-image': `url('${scope.product.files.find(f => f.main).url}')`
    });

    scope.deleteItem = ProductsCtrl.deleteItem;
  }
})();