(function() {
  'use strict';

  angular
    .module('app')
    .component('createProducts', {
      controller: CreateProductsController,
      controllerAs: 'vm',
      templateUrl: 'app/products/create-products.html',
    });

  /** @ngInject */
  CreateProductsController.$inject = ['dataservice', '$state'];

  function CreateProductsController(dataservice, $state) {
    const vm = this;
    vm.product = {
      name: '',
      fileField: '',
      price: ''
    };

    vm.saveProduct = saveProduct;
    vm.assignImage = assignImage;

    function saveProduct() {
      dataservice.createProduct(vm.product).then(() => {
        $state.go('products.list');
      });
    }

    function assignImage(imgUrl) {
      vm.product.fileField = imgUrl;
    }
  }
})();