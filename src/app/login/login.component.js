(function() {
  'use strict';

  angular
    .module('app')
    .component('login', {
      controller: LoginController,
      controllerAs: 'vm',
      templateUrl: 'app/login/login.view.html',
    });

  /** @ngInject */
  LoginController.$inject = ['API_CONSTANT', '$http', '$state'];

  function LoginController(API_CONSTANT, $http, $state) {
    const vm = this;
    vm.loginUser = {};
    vm.errorMsg = '';

    vm.API_CONSTANT = API_CONSTANT;

    vm.submitLogin = submitLogin;
    vm.checkRequired = checkRequired;

    function submitLogin() {
      vm.showRequired = false;
      vm.errorMsg = '';
      if (!vm.loginUser.email || !vm.loginUser.password) {
        vm.showRequired = true;
        return vm.errorMsg = 'Please, fill in required fields';
      }

      $http
        .post(`${vm.API_CONSTANT}/login`, vm.loginUser)
        .then(() => {
          $state.go('products.list');
        })
        .catch(() => {
          vm.errorMsg = 'User Not Found';
        });
    }

    function checkRequired(val) {
      return vm.showRequired && !val;
    }
  }

})();