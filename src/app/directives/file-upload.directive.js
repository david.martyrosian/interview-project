'use strict';

(function() {
  angular
    .module('app')
    .directive('fileUpload', fileUpload);
  fileUpload.$inject = ['dataservice'];

  function fileUpload(dataservice) {
    const directive = {
      restrict: 'A',
      scope: {
        assign: '&onAssign'
      },
      link
    }
    return directive;

    function link (scope, element, attr) {
      element.bind('change', function () {
        var formData = new FormData();
        formData.append('file', element[0].files[0]);
        dataservice.uploadImage(formData).then(imgUrl => {
          scope.assign({imgUrl});
        });
      });
    }
  }
})();
