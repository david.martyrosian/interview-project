(function() {
  'use strict';
  
  angular
      .module('app')
      .factory('dataservice', dataservice);

  dataservice.$inject = ['$http', 'API_CONSTANT', 'logger'];

  function dataservice($http, API_CONSTANT, logger) {
    const service = {
      getProducts,
      deleteProduct,
      createProduct,
      uploadImage
    };

    return service;

    function getProducts() {
      return $http.get(`${API_CONSTANT}/products`)
        .then(getProductsComplete)
        .catch(getProductsFailed);

       function getProductsComplete(resp) {
          return resp.data;
       }

       function getProductsFailed(err) {
          logger.logError(err);
       }
    }

    function deleteProduct(id) {
      return $http.delete(`${API_CONSTANT}/products/${id}`)
        .then(deleteProductsComplete)
        .catch(deleteProductsFailed);

       function deleteProductsComplete(resp) {
          return resp.data;
       }

       function deleteProductsFailed(err) {
          logger.logError(err);
       }
    }

    function uploadImage(formData) {
      return $http({
        url: `${API_CONSTANT}/upload-file/files`,
        method: "POST",
        data: formData,
        headers: {'Content-Type': undefined}
      })
        .then(uploadFileComplete)
        .catch(uploadFileFailed);

       function uploadFileComplete(resp) {
          return resp.data;
       }

       function uploadFileFailed(err) {
          logger.logError(err);
       }
    }

    function createProduct(product) {
      return $http.post(`${API_CONSTANT}/products`, {product})
        .then(createProductsComplete)
        .catch(createProductsFailed);

       function createProductsComplete(resp) {
          return resp.data;
       }

       function createProductsFailed(err) {
          logger.logError(err);
       }
    }
  }
})();