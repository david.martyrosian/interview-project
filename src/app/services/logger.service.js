(function() {
  angular
    .module('app')
    .factory('logger', logger);

  function logger($log) {
    const service = {
     logError
    };
    return service;

    function logError(msg) {
        const loggedMsg = 'Error: ' + msg;
        $log.error(loggedMsg);
        return loggedMsg;
    }
  }
})();