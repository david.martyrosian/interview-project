(function() {
  'use strict';

  angular
    .module('app')
    .filter('priceFilter', priceFilter);

  function priceFilter() {
    return function(input, currency) {
      let price = Number(input);
      if (currency === 'USD') {
        price = price / 25;
      }
      return `${price} ${currency}`;
    };
  }
})();