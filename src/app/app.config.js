(function() {
  'use strict';

  angular.module('app').config(configBlock);

  /** @ngInject */
  function configBlock($locationProvider) {
    $locationProvider.
        html5Mode(true);
  }

})();
